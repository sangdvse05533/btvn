/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package countvowels;

/**
 *
 * @author Sang
 */
public class Countvowels {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String s = "HELLO HOW ARE YOU TODAY";
        check(s);
    }
    
    static void check(String s){
        String s1 = s.toLowerCase();
        int countv =0;
        int countc =0;
        for (int i = 0; i < s1.length(); i++) {            
            if(s1.charAt(i)=='a' || s1.charAt(i)=='e' || s1.charAt(i)=='i' || s1.charAt(i)=='u'
                    || s1.charAt(i)=='o'){
                countv++;
            }else if(s1.charAt(i) >= 'a' && s1.charAt(i) <= 'z'){
                countc++;
            }
        }
        System.out.println("Voweles: "+countv);
        System.out.println("Consonant: "+countc);
    }
}
