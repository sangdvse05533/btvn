/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package checkonlydigits;

/**
 *
 * @author Sang
 */
public class CheckOnlyDigits {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String s1 = "HiIam14yearsold";
        System.out.println(checkDigits(s1));
        String s2 ="2011998";
        System.out.println(checkDigits(s2));
    }
    static boolean checkDigits(String s){
        for (char c : s.toCharArray()) {            
            if(c < '0' || c > '9'){
                return false;
            }
        }
        return true;
    }
}
