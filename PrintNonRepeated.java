/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package printnonrepeated;

/**
 *
 * @author Sang
 */
public class PrintNonRepeated {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String s = "HeelloIamHungo";
        printNonRepeated(s);
    }
    
    static void printNonRepeated(String s){
        for(Character ch : s.toCharArray()) {
            if(s.indexOf(ch) == s.lastIndexOf(ch)) {
                System.out.println("First non repeat character = " + ch);
                break;
            }
        }               
    }
}
