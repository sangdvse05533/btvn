/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservestring;

/**
 *
 * @author Sang
 */
public class ReverseString {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String s = "HelloWorld";
        System.out.print(reverseArray(s));
    }
    static String reverseArray(String arr){
        if(arr.isEmpty()){
            return arr;
        }
        return reverseArray(arr.substring(1)) + arr.charAt(0);
    }
    
}
