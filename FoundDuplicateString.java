/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foundduplicatestring;

/**
 *
 * @author Sang
 */
public class FoundDuplicateString {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String s = "Great responsibility";
        checkDuplicate(s);
    }

    static void checkDuplicate(String s) {        
        char[] arrayString = s.toCharArray();
        for (int i = 0; i < arrayString.length; i++) {
            int count =1;
            for (int j = i +1; j < arrayString.length; j++) {
                if (arrayString[i] == arrayString[j]) {
                    count++;           
                    arrayString[j] = '0';
                }
            }
            if(count > 1 && arrayString[i] != '0'){
                System.out.println(arrayString[i]);
            }
        }
    }
}
