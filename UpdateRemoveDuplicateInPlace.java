/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package removeduplicateinplace;

import java.util.Arrays;

/**
 *
 * @author Sang
 */
public class UpdateRemoveDuplicateInPlace {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] arr = {1, 3, 3, 6, 7, 7, 7, 8, 3};
        removeDeplicate(arr);
        
    }

    static void removeDeplicate(int[] arr) {
        System.out.println("Original Array : ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "\t");
        }
        int l = arr.length;
        for (int i = 0; i < l; i++) {
            for (int j = i + 1; j < l; j++) {
                if (arr[i] == arr[j]) {
                    for (int k = j; k < l - 1; k++) {
                        arr[k] = arr[k + 1];
                    }
                    l--;
                    j--;
                }
            }
        }
        int[] array1 = Arrays.copyOf(arr, l);
        System.out.println();
        System.out.println("Array with unique values : ");
        for (int i = 0; i < array1.length; i++) {
            System.out.print(array1[i] + "\t");
        }
        System.out.println();

    }
}
